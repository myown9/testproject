package DataRandomization;

import java.nio.charset.Charset;
import java.util.Random;

public class UsingCharacterSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte[] array = new byte[256];
		new Random().nextBytes(array);
		String randomString = new String(array, Charset.forName("UTF-8"));
		int n=10;
		StringBuffer r = new StringBuffer(); 
		for (int k = 0; k < randomString.length(); k++) { 
			  
            char ch = randomString.charAt(k); 
  
            if (((ch >= 'a' && ch <= 'z') 
                 || (ch >= 'A' && ch <= 'Z') 
                 || (ch >= '0' && ch <= '9')) 
                && (n > 0)) { 
  
                r.append(ch); 
                n--; 
        
           }

		}
		 System.out.println(r.toString());
	}

}
